# weather_noaa_data

A python module to retrieve NOAA weather data and station information.  Utilizes the ish_parser library and builds on that with some convenient data functions.

## Documentation

- **getFileList(year)** - Get the list of .gz files for the year

- **readFTPFile(noaa_filename)** - pull a weather file from the NOAA site, and read it in memory

- **downloadFile(noaa_filename,save_filename)** - download and save a weather file from the NOAA site

- **getStationInfo(filename=None,last_year = None)** - retrieve a dictionary of all the NOAA stations, their names and locations
    - filename - optional use a file on your computer, otherwise will pull live from NOAA site
    - last_year - only pull stations that have data since *last_year*

- **readFile(filename)** - read a local file and get the gz_data (to pass to other functions)

- **getIshReports(gz_data)** - get a list of the ish_parser report objects from  .gz data using readFile or readFTPFile and get a list of the ish_parser report objects

- **getData(gz_data)** - a function that gets many of the fields from the weather data and returns a dictionary object
    - some fields return the data quality
        - see qualityD for the key

- **qualityD** - a dictionary of the quality codes

Example:

        # Read from local file
        dataD = noaa_weather.getData(noaa_weather.readFile('999999-00415-2016.gz'))
        
        # or 
        
        # Read directly from NOAA
        dataD = noaa_weather.getData(noaa_weather.readFTPFile('999999-00415-2016.gz'))

Output Is:

        dataD = {
            station_info: {
                station: '999999-00415'
                elevation: 60
                latitude: 29.845
                longitude: -82.048
                report_type: 'FM-15'
            }
            data: {
                air_temperature: [22.0, 22.0, 21.0, 21.0, 22.0]
                air_temperature_quality: [u'C', u'5', u'5', u'C', u'5']
                wind_speed: [0.0, 1.5, 1.5, 0.0, 0.0]
                wind_direction: [999, 240, 230, 999, 999]
                datetime: [datetime.datetime(2016, 1, 1, 3, 55, tzinfo=<UTC>), datetime.datetime(2016, 1, 1, 4, 15, tzinfo=<UTC>), datetime.datetime(2016, 1, 1, 4, 35, tzinfo=<UTC>), datetime.datetime(2016, 1, 1, 4, 55, tzinfo=<UTC>), datetime.datetime(2016, 1, 1, 5, 15, tzinfo=<UTC>)]
                precipitation: [None, None, None, None, None]
                dew_point: [22.0, 21.0, 22.0, 21.0, 21.0]
                dew_point_quality: [u'C', u'5', u'5', u'C', u'5']
                humidity: [100.0, 100.0, 100.0, 100.0, 100.0]
                sea_level_pressure: [9999.9, 9999.9, 9999.9, 9999.9, 9999.9]
                sea_level_pressure_quality: [u'9', u'9', u'9', u'9', u'9']
                sky_cover: [[{'cloud_type': MISSING, 'base_height': 99999, 'coverage': 00}], [{'cloud_type': MISSING, 'base_height': 99999, 'coverage': 00}], [{'cloud_type': MISSING, 'base_height': 99999, 'coverage': 00}], [{'cloud_type': 59, 'base_height': 30, 'coverage': 07}], [{'cloud_type': 59, 'base_height': 61, 'coverage': 08}]]
            }
        }