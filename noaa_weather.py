import urllib.request, gzip,sys,os
from .ish_parser import ish_parser
from io import BytesIO

version = '1.0'

qualityD = {
    '0': 'Passed gross limits check',
    '1': 'Passed all quality control checks',
    '2': 'Suspect',
    '3': 'Erroneous',
    '4': 'Passed gross limits check , data originate from an NCDC data source',
    '5': 'Passed all quality control checks, data originate from an NCDC data source',
    '6': 'Suspect, data originate from an NCDC data source',
    '7': 'Erroneous, data originate from an NCDC data source',
    '9': 'Passed gross limits check if element is present',
    'A': 'Data value flagged as suspect, but accepted as a good value',
    'C': 'Temperature and dew point received from Automated Weather Observing System (AWOS) are reported in whole degrees Celsius. Automated QC flags these values, but they are accepted as valid.',
    'I': 'Data value not originally in data, but inserted by validator',
    'M': 'Manual changes made to value based on information provided by NWS or FAA',
    'P': 'Data value not originally flagged as suspect, but replaced by validator',
    'R': 'Data value replaced with value computed by NCDC software',
    'U': 'Data value replaced with edited value',
}

quality_bad = ['2','3','6','7']

#---NOAA Site Functions
def getFileList(year):
    '''Get list of Station Files for the year'''
    response = urllib.request.urlopen('ftp://ftp.ncdc.noaa.gov/pub/data/noaa/{0}'.format(year))
    
    stations = []
    for line in response.read().splitlines():
        cols = line.split('.gz">')
        if len(cols)> 0:
            station = cols[-1].split(' ')[-1]
            stations.append(station)
    
    return stations

def readFTPFile(noaa_filename):
    '''Read NOAA weather data file from ftp in format 'station-year.gz' '''
    year = noaa_filename[-7:-3]
    response = urllib.request.urlopen('ftp://ftp.ncdc.noaa.gov/pub/data/noaa/{0}/{1}'.format(year,noaa_filename))
    file_io = BytesIO(response.read())
    file_gz = gzip.GzipFile(fileobj=file_io, mode='rb')
    
    return bytes.decode(file_gz.read())
        
def downloadFile(noaa_filename,save_filename):
    '''Download NOAA weather data file in format 'station-year.gz' '''
    year = noaa_filename[-7:-3]
    response = urllib.request.urlopen('ftp://ftp.ncdc.noaa.gov/pub/data/noaa/{0}/{1}'.format(year,noaa_filename))
    with open(save_filename,'wb') as save_file:
        save_file.write(response.read())

def downloadStation(station,year,folder):
    '''Download NOAA weather data file for a station and year'''
    station = station.strip().replace(' ','-') # handle spaced station name
    print('ftp://ftp.ncdc.noaa.gov/pub/data/noaa/{0}/{1}-{0}.gz'.format(year,station))
    response = urllib.request.urlopen('ftp://ftp.ncdc.noaa.gov/pub/data/noaa/{0}/{1}-{0}.gz'.format(year,station))
    with open(os.path.join(folder,'{1}-{0}.gz'.format(year,station)),'wb') as save_file:
        save_file.write(response.read())

def getStationInfo(isd_history_file=None,last_year = None):
    '''Get Station info from a isd-histor.txt file or from the web'''
    if isd_history_file == None:
        response = urllib.request.urlopen('ftp://ftp.ncdc.noaa.gov/pub/data/noaa/isd-history.txt')
        txt = response.read()
    else:
        with open(isd_history_file,'r') as file:
            txt = file.read()
    
    stationD = {}
    for line in txt.splitlines()[22:]:
        station = line[:12].replace(' ','-')
        year = int(line[91:95])
        
        if year >= last_year:
            stationD[station]={
                'name':line[13:43].strip(),
                'country':line[43:47].strip(),
                'state':line[48:50].strip(),
                'latitude':None,
                'longitude':None,
                'elevation':None,
            }
            try:
                stationD[station]['latitude'] = float(line[57:65].strip())
            except: pass
            try:
                stationD[station]['longitude'] = float(line[65:74].strip())
            except: pass
            try:
                stationD[station]['elevation'] = float(line[74:82].strip())
            except: pass
        
    return  stationD

#--- Data Functions
def readFile(filename):
    '''Get report object from ish_parser'''
    with gzip.open(filename,'rb') as file:
        gz_data = bytes.decode(file.read())
    return gz_data

def getIshReports(gz_data):
    '''Get report object from ish_parser'''
    wf = ish_parser()
    wf.loads(gz_data)
    return wf.get_reports()

def getData(gz_data=None, filename=None):
    if filename != None:
        gz_data = readFile(gz_data)
    rpts = getIshReports(gz_data)
    
    dataD = {
        'station_info':{
            'station': rpts[0].weather_station+'-'+rpts[0].wban,
            'latitude': rpts[0].latitude,
            'longitude': rpts[0].longitude,
            'elevation': rpts[0].elevation,
            'report_type': rpts[0].raw[41:46],
        },
        
        'data':{
            'datetime': [],
            'air_temperature': [],
            'wind_speed': [],
            'wind_direction': [],
            'precipitation': [],
            'sky_cover': [],
            'dew_point': [],
            'humidity': [],
            'sea_level_pressure': [],
            'air_temperature_quality': [],
            'sea_level_pressure_quality': [],
            'dew_point_quality': [],
        }
    }
    
    for rpt in rpts:
        dataD['data']['datetime'].append(rpt.datetime)
        dataD['data']['air_temperature'].append(rpt.air_temperature)
        dataD['data']['wind_speed'].append(rpt.wind_speed)
        dataD['data']['wind_direction'].append(rpt.wind_direction)
        dataD['data']['precipitation'].append(rpt.precipitation)
        dataD['data']['sky_cover'].append(rpt.sky_cover)
        dataD['data']['dew_point'].append(rpt.dew_point)
        dataD['data']['humidity'].append(rpt.humidity.humidity)
        dataD['data']['sea_level_pressure'].append(rpt.sea_level_pressure)
        
        # Quality Checks
        dataD['data']['air_temperature_quality'].append(rpt.raw[92])
        dataD['data']['sea_level_pressure_quality'].append(rpt.raw[104])
        dataD['data']['dew_point_quality'].append(rpt.raw[98])
        
    return dataD
